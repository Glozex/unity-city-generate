﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class double222 : MonoBehaviour {

    public Material mat, mat2, mat3;
    private Random rand = new Random();

    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < 3; i++)
        {
            for (int z = 0; z < 3 +  Random.Range(1, 4); z++)
        {
            for (int y = 0; y < 3; y++)
            {
                for (int x = 0; x < 3; x++)
                {
                    GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    cube.AddComponent<Rigidbody>();

                    cube.GetComponent<Renderer>().material = GetRandomMat();
                    cube.transform.position = new Vector3(x+i*10, 0.5f + z*1f, y);
                }
            }
        }
    }
}
    public Material GetRandomMat()
    {
        int n = Random.Range(1, 4);

        if (n == 1) return mat;

        if (n == 2) return mat2;

        return mat3;
    }
}
