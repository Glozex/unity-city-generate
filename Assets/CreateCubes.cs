﻿using UnityEngine;
using System.Collections;


public class CreateCubes : MonoBehaviour {

	public Material mat;

	// Use this for initialization
	void Start ()
	{
		//mat = Resources.Load("Cliff") as Material;
		Create10Cubes();
	}


	
	[ContextMenu("CreateCubes")]
	private void Create10Cubes()
	{
		Vector3 position = new Vector3 (0, 0, 0);
		//		Quaternion rotation = new Quaternion (0, 0, 0 , 0);
		float radius = 5f;
		float cubesNumber = 28;
		
		for (var n = 0; n < 100; n++) {
			for (var i = 1; i <= cubesNumber; i ++) {
				var x = radius * Mathf.Cos ( (i + n/2f) * Mathf.PI * 2 / cubesNumber  );
				var y = radius * Mathf.Sin ( (i + n/2f) * Mathf.PI * 2 / cubesNumber  );
				
				
				GameObject cube = GameObject.CreatePrimitive (PrimitiveType.Cube);
				position = new Vector3 (20 + x * 1f, 0.5f + n * 1.0f, 20 + y * 1f);
				 
				cube.transform.position = position;
				
				cube.GetComponent<Renderer>().material = mat;
				
				//Quaternion rotateY = Quaternion.AngleAxis( - (360 / 19 * i + n * 13F), Vector3.up);
				Quaternion rotateY = Quaternion.AngleAxis( - (360 / cubesNumber * (i + n/2f) ), Vector3.up);
				Quaternion rotateX = Quaternion.AngleAxis(0, Vector3.right);
				cube.transform.rotation = rotateY * rotateX;
				
				Rigidbody gameObjectsRigidBody = cube.AddComponent<Rigidbody> (); // Add the rigidbody.
				gameObjectsRigidBody.mass = 1; 
			}
			
		}
	}
}