﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class doublecube : MonoBehaviour
{

    void Start()
    {
        Material newMat = Resources.Load("cube_1", typeof(Material)) as Material;
        Material newMat2 = Resources.Load("cube_2", typeof(Material)) as Material;
        Material newMat3 = Resources.Load("cube_3", typeof(Material)) as Material;


        for (int y = 0; y < 3; y++)
        {
            for (int x = 0; x < 3; x++)
            {
                GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                cube.AddComponent<Rigidbody>();

                cube.GetComponent<Renderer>().material = newMat;
                cube.transform.position = new Vector3(x, y, 0);
            }
        }
    }
}
